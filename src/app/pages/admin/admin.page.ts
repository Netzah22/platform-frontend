import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { RegistroEService } from '../../services/registro-e.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {

  admin:any;

  constructor(private regS: RegistroEService, private alertCtrl: AlertController, private router: Router) { }

  ngOnInit() {
    this.crearForm();
  }

  crearForm(){
    this.admin = new FormGroup({
      nombre: new FormControl(''),
      primer_apellido: new FormControl(''),
      segundo_apellido: new FormControl(''),
      matricula: new FormControl('admin'),
      contra: new FormControl('admin')
    });
  }

  enviar(){
    this.regS.update(this.admin.value).subscribe(
      (data:any)=>{
        console.log(data);
        this.presentAlert();
      }
    )
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Se ha actualizado la información',
      buttons: [
        {
          text: 'Ok',
          role: 'confirm',
          handler: ()=>{
            this.admin.reset();
          }
        }
      ]
    });

    await alert.present();
  }

}
