import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RegistroEService } from '../../services/registro-e.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-detalle-tutoria',
  templateUrl: './detalle-tutoria.page.html',
  styleUrls: ['./detalle-tutoria.page.scss'],
})
export class DetalleTutoriaPage implements OnInit {

  constructor(private route: ActivatedRoute, private regS: RegistroEService) { }

  tutoria;
  nombreTutoria;
  infoTuto:any;

  ngOnInit(){
    this.getCurrentTuto();
  }

  getCurrentTuto(){
    this.tutoria = this.route.snapshot.paramMap.get('id');
    console.log(this.tutoria);

    this.getTuto();
    
  }

  getTuto(){
    this.regS.getTuto(this.tutoria).subscribe(
      (data:any)=>{
        let nombre = data.data;
        this.nombreTutoria = nombre["nombre"];
        console.log(this.nombreTutoria);
        this.getTutoAlumno();
      }
    )
  }

  getTutoAlumno(){
    this.regS.getTutoAlumno(this.nombreTutoria).subscribe(
      (data:any)=>{
        this.infoTuto = data.data;
        console.log(this.infoTuto);
      }
    )
  }

  downloadPDF(){
    const DATA = document.getElementById("htmlTable");
    const doc = new jsPDF('p', 'pt', 'a4');

    const options = {
      background: 'white',
      scale: 3
    };

    html2canvas(DATA, options).then((canvas) => {

      const img = canvas.toDataURL('image/PNG');

      const bufferX = 15;
      const bufferY = 15;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() + 3.8 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      docResult.save(`${new Date().toISOString()}_lista.pdf`);
    });
  }

}
