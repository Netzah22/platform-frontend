import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleTutoriaPageRoutingModule } from './detalle-tutoria-routing.module';

import { DetalleTutoriaPage } from './detalle-tutoria.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleTutoriaPageRoutingModule,
    ComponentsModule
  ],
  declarations: [DetalleTutoriaPage]
})
export class DetalleTutoriaPageModule {}
