import { Component, OnInit } from '@angular/core';
import { RegistroEService } from '../../services/registro-e.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-alumnos',
  templateUrl: './alumnos.page.html',
  styleUrls: ['./alumnos.page.scss'],
})
export class AlumnosPage implements OnInit {

  constructor(private regS: RegistroEService) { }


  tutorias:any;

  ngOnInit() {
    this.getAllTuto();
  }

  getAllTuto(){
    this.regS.getAllTuto().subscribe(
      (data:any)=>{
        this.tutorias = data.data;
        console.log(this.tutorias);
      }
    )
  }

  downloadPDF(){
    const DATA = document.getElementById("htmlTable");
    const doc = new jsPDF('p', 'pt', 'a4');

    const options = {
      background: 'white',
      scale: 3
    };

    html2canvas(DATA, options).then((canvas) => {

      const img = canvas.toDataURL('image/PNG');

      const bufferX = 15;
      const bufferY = 15;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() + 18 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      docResult.save(`${new Date().toISOString()}_lista.pdf`);
    });
  }

}
