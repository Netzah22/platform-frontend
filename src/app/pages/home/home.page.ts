import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistroEService } from '../../services/registro-e.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  ngOnInit(){
    this.createForm();
  }

  constructor(private regS: RegistroEService, private router: Router) {}

  login: FormGroup;

  createForm(){
    this.login = new FormGroup({
      matricula: new FormControl('', Validators.required),
      contra: new FormControl('', Validators.required)
    });
  }

  logueo(m){
    this.regS.login(this.login.value).subscribe(
      (data:any)=>{
        console.log(data);
        if(data.text === "Login correcto" && data.matricula === "adminoo"){
          this.router.navigateByUrl('/inicio');
        }else if(data.text === "Login correcto" && data.matricula === this.login.value.matricula){
          this.router.navigateByUrl('/inicio-alumno/' + m);
        }else{
          console.log("Logueo incorrecto");
        }
      }
    )
  }

}
