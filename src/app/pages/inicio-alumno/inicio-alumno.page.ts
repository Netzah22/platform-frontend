import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RegistroEService } from '../../services/registro-e.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-inicio-alumno',
  templateUrl: './inicio-alumno.page.html',
  styleUrls: ['./inicio-alumno.page.scss'],
})
export class InicioAlumnoPage implements OnInit {

  tutorias:any;
  currentUser: any;
  tutoInfo: any;
  newTutoInfo: any;
  infoIns: any;
  arr: any;
  ins;

  constructor(private regS: RegistroEService, private route: ActivatedRoute, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.getData();
    this.getInfoUser();
  }

  comprobarInscripcion(){  
    this.regS.comprobe(this.currentUser["matricula"]).subscribe(
      (data:any)=>{
        this.ins = data;
        console.log(this.ins["text"]);
      }
    )
  }

  getData(){
    this.regS.getAll().subscribe(
      (data:any)=>{
        this.tutorias = data.data;
      }
    )
  }

  getInfoUser(){
    let matricula:string = this.route.snapshot.paramMap.get('m');
    
    this.regS.getOne(matricula).subscribe(
      (data:any)=>{
        this.currentUser = data.data;
        // console.log(this.currentUser);
      }
    )
  }



  getTutoria(id){
    this.comprobarInscripcion();
    if(this.ins["text"] === "Alumno inscrito"){
      this.inscrito();
    }else{
      this.regS.getTuto(id+1).subscribe(
        (data:any)=>{
          this.tutoInfo = data.data;
        }
      );
      this.presentAlert(id);
    }
  }



  inscribir(id){
    this.getInfoUser();
    this.infoIns = this.currentUser;
    
    let nombre = this.infoIns["nombre"];
    let apellidos = this.infoIns["primer_apellido"] + " " + this.infoIns["segundo_apellido"];
    let carrera = this.infoIns["carrera"];
    let grupo = this.infoIns["grupo"];
    let matricula = this.infoIns["matricula"];
    let numero = this.infoIns["numero"];

    // this.getTutoria(id);
    this.newTutoInfo = this.tutoInfo;
    let nombreT = this.newTutoInfo["nombre"];
    let encargado = this.newTutoInfo["encargado"];

    this.arr = {
      nombre:nombre,
      apellidos:apellidos,
      carrera:carrera,
      grupo:grupo,
      matricula:matricula,
      numero:numero,
      nombre_tutoria:nombreT,
      encargado_tutoria:encargado
    }

    this.regS.alumnoTutoria(this.arr).subscribe(
      (data:any)=>{
        console.log("Se ha registrado a una tutoria");
        this.confirm();
      }
    )
  }

  async presentAlert(id) {
    const alert = await this.alertCtrl.create({
      header: '¿Desea inscribirse a esta tutoría?',
      buttons: [
        {
          text: 'Si',
          role: 'confirm',
          handler: ()=>{
            this.inscribir(id);
          }
        },
        {
          text: 'No',
          role: 'cancel'
        }
      ]
    });

    await alert.present();
  }

  async confirm() {
    const alert = await this.alertCtrl.create({
      header: '¡Se ha inscrito a una tutoría!',
      buttons: [
        {
          text: 'Ok',
          role: 'confirm',
          handler: ()=>{
          }
        }
      ]
    });

    await alert.present();
  }

  async inscrito() {
    const alert = await this.alertCtrl.create({
      header: 'Actualmente se encuentra inscrito en ' + this.ins.data["nombre_tutoria"],
      buttons: [
        {
          text: 'Ok',
          role: 'confirm',
          handler: ()=>{
          }
        }
      ]
    });

    await alert.present();
  }

}
