import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { RegistroEService } from 'src/app/services/registro-e.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  formulario: FormGroup;

  constructor(private regS: RegistroEService, private alertCtrl: AlertController, private router: Router) { }

  ngOnInit() {
    this.createForm();
  }

  createForm(){
    this.formulario = new FormGroup({
      nombre: new FormControl('', Validators.required),
      primer_apellido: new FormControl('', Validators.required),
      segundo_apellido: new FormControl('', Validators.required),
      carrera: new FormControl('', Validators.required),
      grupo: new FormControl('', Validators.required),
      matricula: new FormControl('', Validators.required),
      numero: new FormControl('', Validators.required),
      contra: new FormControl('', Validators.required)
    });
  }

  enviar(){
    this.regS.enviarReg(this.formulario.value).subscribe(
      (data:any)=>{
        console.log(data);
      }
    )
    this.presentAlert();
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Se ha registrado correctamente',
      buttons: [
        {
          text: 'Ok',
          role: 'confirm',
          handler: ()=>{
            this.formulario.reset();
            this.router.navigateByUrl('/home');
          }
        }
      ]
    });

    await alert.present();
  }
}