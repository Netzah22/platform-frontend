import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListasPage } from './listas.page';

const routes: Routes = [
  {
    path: '',
    component: ListasPage
  },
  {
    path: 'agricultura',
    loadChildren: () => import('./agricultura/agricultura.module').then( m => m.AgriculturaPageModule)
  },
  {
    path: 'desarrollo',
    loadChildren: () => import('./desarrollo/desarrollo.module').then( m => m.DesarrolloPageModule)
  },
  {
    path: 'moda',
    loadChildren: () => import('./moda/moda.module').then( m => m.ModaPageModule)
  },
  {
    path: 'energias',
    loadChildren: () => import('./energias/energias.module').then( m => m.EnergiasPageModule)
  },
  {
    path: 'gastro',
    loadChildren: () => import('./gastro/gastro.module').then( m => m.GastroPageModule)
  },
  {
    path: 'meca',
    loadChildren: () => import('./meca/meca.module').then( m => m.MecaPageModule)
  },
  {
    path: 'procesos',
    loadChildren: () => import('./procesos/procesos.module').then( m => m.ProcesosPageModule)
  },
  {
    path: 'ti',
    loadChildren: () => import('./ti/ti.module').then( m => m.TiPageModule)
  },
  {
    path: 'agregar-listas',
    loadChildren: () => import('./agregar-listas/agregar-listas.module').then( m => m.AgregarListasPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListasPageRoutingModule {}
