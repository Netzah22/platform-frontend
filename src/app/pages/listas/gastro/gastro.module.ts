import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GastroPageRoutingModule } from './gastro-routing.module';

import { GastroPage } from './gastro.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GastroPageRoutingModule,
    ComponentsModule
  ],
  declarations: [GastroPage]
})
export class GastroPageModule {}
