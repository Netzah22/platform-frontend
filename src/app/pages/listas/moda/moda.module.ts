import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModaPageRoutingModule } from './moda-routing.module';

import { ModaPage } from './moda.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModaPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ModaPage]
})
export class ModaPageModule {}
