import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgregarListasPage } from './agregar-listas.page';

const routes: Routes = [
  {
    path: '',
    component: AgregarListasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgregarListasPageRoutingModule {}
