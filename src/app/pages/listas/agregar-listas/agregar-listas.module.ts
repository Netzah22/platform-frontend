import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgregarListasPageRoutingModule } from './agregar-listas-routing.module';

import { AgregarListasPage } from './agregar-listas.page';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgregarListasPageRoutingModule,
    ComponentsModule
  ],
  declarations: [AgregarListasPage]
})
export class AgregarListasPageModule {}
