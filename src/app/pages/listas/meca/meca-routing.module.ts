import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MecaPage } from './meca.page';

const routes: Routes = [
  {
    path: '',
    component: MecaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MecaPageRoutingModule {}
