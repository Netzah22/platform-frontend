import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TiPage } from './ti.page';

const routes: Routes = [
  {
    path: '',
    component: TiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TiPageRoutingModule {}
