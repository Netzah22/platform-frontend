import { Component, OnInit } from '@angular/core';
import { RegistroEService } from '../../services/registro-e.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  tuto:any;

  constructor(private regS: RegistroEService, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.getData();
  }

  getData(){
    this.regS.getAll().subscribe(
      (data:any)=>{
        this.tuto = data.data;
        console.log(this.tuto);
      }
    )
  }


  eliminar(index){    
    this.presentAlert(index);
  }

  drop(index){
    this.regS.eliminar(index).subscribe(
      (data:any)=>{
        console.log(data);
      }
    );
  }

  async presentAlert(index) {
    const alert = await this.alertCtrl.create({
      header: '¿Desea eliminar esta tutoría?',
      buttons: [
        {
          text: 'Ok',
          role: 'confirm',
          handler: ()=>{
            this.drop(index);
            this.confirm();
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });

    await alert.present();
  }

  async confirm() {
    const alert = await this.alertCtrl.create({
      header: 'Se ha eliminado la tutoria',
      buttons: [
        {
          text: 'Ok',
          role: 'confirm',
          handler: ()=>{
            window.location.reload();
          }
        }
      ]
    });

    await alert.present();
  }

}
