import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TutoriaPageRoutingModule } from './tutoria-routing.module';

import { TutoriaPage } from './tutoria.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TutoriaPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule
  ],
  declarations: [TutoriaPage]
})
export class TutoriaPageModule {}
