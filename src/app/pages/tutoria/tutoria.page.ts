import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RegistroEService } from '../../services/registro-e.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tutoria',
  templateUrl: './tutoria.page.html',
  styleUrls: ['./tutoria.page.scss'],
})
export class TutoriaPage implements OnInit {

  constructor(private regS: RegistroEService, private alertCtrl: AlertController, private router: Router) { }

  ngOnInit() {
    this.createForm();
  }

  tutoria;

  createForm(){
    this.tutoria = new FormGroup({
      nombre: new FormControl('', Validators.required),
      encargado: new FormControl('', Validators.required),
      descripcion: new FormControl('', Validators.required),
      limitar: new FormControl,
      limite: new FormControl(false),
      dias: new FormGroup({
        lunest: new FormGroup({
          lunes: new FormControl(false),
          lunestime: new FormControl()
        }),

        martest: new FormGroup({
          martes: new FormControl(false),
          martestime: new FormControl()
        }),

        miercolest: new FormGroup({
          miercoles: new FormControl(false),
          miercolestime: new FormControl()
        }),

        juevest: new FormGroup({
          jueves: new FormControl(false),
          juevestime: new FormControl()
        }),

        viernest: new FormGroup({
          viernes: new FormControl(false),
          viernestime: new FormControl()
        }),

        sabadot: new FormGroup({
          sabado: new FormControl(false),
          sabadotime: new FormControl()
        }),
      })
    });
  }

  enviar(){
    console.log(this.tutoria.value);
    
    this.regS.newTutoria(this.tutoria.value).subscribe(
      (data: any)=>{
          if(data.ok === true){
            this.presentAlert();
          }
        console.log(data);
      }
    )
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Se ha agregado una nueva tutoría',
      buttons: [
        {
          text: 'Ok',
          role: 'confirm',
          handler: ()=>{
            this.tutoria.reset();
          }
        }
      ]
    });

    await alert.present();
  }

}
