export interface newSign{
    nombre: string,
    primer_apellido: string,
    segundo_apellido: string,
    carrera: string,
    grupo: string,
    matricula: string,
    numero: number,
    contra: string
}

export interface login{
    matricula: string,
    contra: string
}

export interface newTuto{
    nombre: string,
    encargado: string,
    descripcion: string,
    limitar:boolean,
    limite: number,
    dias: JSON
    // {
    //     lunes:boolean,
    //     martes:boolean,
    //     miercoles:boolean,
    //     jueves:boolean,
    //     viernes:boolean,
    //     sabado:boolean
    // }
}

export interface update{
    nombre: string,
    primer_apellido: string,
    segundo_apellido: string,
    matricula: string,
    contra: string
}

export interface alumnoTutoria{
    nombre: string,
    apellidos: string,
    carrera: string,
    grupo: string,
    matricula: string,
    numero: number,
    nombre_tutoria: string,
    encargado_tutoria: string
}