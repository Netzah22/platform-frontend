import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RegistroEService } from '../../services/registro-e.service';

@Component({
  selector: 'app-header-alumno',
  templateUrl: './header-alumno.component.html',
  styleUrls: ['./header-alumno.component.scss'],
})
export class HeaderAlumnoComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private regS: RegistroEService) { }

  currentUser: any;

  ngOnInit() {
    this.getInfoUser();
  }

  inicio(){
    this.router.navigateByUrl('/inicio-alumno');
  }

  logout(){
    console.log('cierre de sesión');
    this.router.navigateByUrl('/home');
  }

  myFunction(){
    let x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }


  getInfoUser(){
    let matricula:string = this.route.snapshot.paramMap.get('m');
    
    this.regS.getOne(matricula).subscribe(
      (data:any)=>{
        this.currentUser = [data.data];
        console.log(this.currentUser);
        
      }
    )
  }

}
