import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';
import { HeaderAlumnoComponent } from './header-alumno/header-alumno.component';



@NgModule({
  declarations: [
    HeaderComponent,
    HeaderAlumnoComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    HeaderComponent,
    HeaderAlumnoComponent
  ]
})
export class ComponentsModule { }
