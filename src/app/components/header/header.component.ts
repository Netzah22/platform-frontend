import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    let ubicacion = window.location.pathname;
    console.log(ubicacion);
    
  }

  // myFunction(){
  //   var x = document.getElementById("myTopnav");
  //   if (x.className === "topnav") {
  //     x.className += " responsive";
  //   } else {
  //     x.className = "topnav";
  //   }
  // }

  myFunction(){
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }

  inicio(){
    this.router.navigateByUrl('/inicio');
  }

  tutoria(){
    this.router.navigateByUrl('/tutoria');
  }

  alumnos(){
    this.router.navigateByUrl('/alumnos');
  }

  listas(){
    this.router.navigateByUrl('/listas');
  }

  admin(){
    this.router.navigateByUrl('/admin');
  }

  logout(){
    console.log('cierre de sesión');
    this.router.navigateByUrl('/home');
  }

}
