import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { newSign, login, newTuto, update, alumnoTutoria } from '../models/post.interface';
import { retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegistroEService {

  constructor(private http:HttpClient) { }

  URL = "http://localhost:3000/";

  login(login: login):Observable<Request>{
    return this.http.post<Request>(`${this.URL}platform/auth`, login);
  }

  enviarReg(newSign: newSign){
    return this.http.post(`${this.URL}platform/registro`, newSign);
  }

  newTutoria(newTuto: newTuto){
    return this.http.post(`${this.URL}platform/addTuto`, newTuto);
  }

  getAll(){
    return this.http.get(`${this.URL}platform/routess/list`);
  }

  update(up: update){
    return this.http.post(`${this.URL}platform/update`, up);
  }

  getOne(matricula){
    return this.http.get(`${this.URL}platform/routess/getOne/` + matricula);
  }

  getTuto(id){
    return this.http.get(`${this.URL}platform/routess/getTuto/` + id);
  }

  alumnoTutoria(alumno: alumnoTutoria){
    return this.http.post(`${this.URL}platform/alumnoTutoria`, alumno);
  }

  comprobe(matricula){
    return this.http.get(`${this.URL}platform/routess/comprobe/` + matricula);
  }

  getAllTuto(){
    return this.http.get(`${this.URL}platform/routess/lista`);
  }

  getTutoAlumno(nombre){
    return this.http.get(`${this.URL}platform/routess/getTutoAlumno/` + nombre);
  }

  eliminar(id){
    return this.http.delete(`${this.URL}platform/routess/eliminar/` + id);
  }
}
