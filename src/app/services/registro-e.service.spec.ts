import { TestBed } from '@angular/core/testing';

import { RegistroEService } from './registro-e.service';

describe('RegistroEService', () => {
  let service: RegistroEService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RegistroEService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
